1. Project Summary:




2. HW description>
	
	2.1 Links to MCUs datasheet
	
		- PIC32MX470
		  http://www.microchip.com.tw/Data_CD/Datasheet/32-Bits/61185B.pdf

		-PIC32MX470 Curiosity
		  https://www.mouser.com/datasheet/2/268/70005283A-1075423.pdf


3. Serial commands: 

	3.1 Link commands source/header file

	3.2 Serial command file. Program.
		Docklight
		https://docklight.de/downloads/

	3.3 Serial Configuration UART1:
		Baudrate: 115200
		Data Bits: 8
		Parity:	None
		Stop Bits: 1
 
		
4. Prerequisites:
	
	4.1 SDK version: 3.61

	4.2 IDE: MPLAB X IDE

	4.3 Compiler: XC32 v1.42

	4.4 Project Configuration:
		
		Device: PIC32MX470F512H
		Hardware Tool: Starter Kit PIC32MX470 Family 

5. Versioning: 
	
	5.1 V1.0.20191211

	5.2 (BRIEF)

6. Authors: 

	6.1 Project staff
		Author: Alejandro Giraldo Martinez
		
	6.2 Maintainer contact:
		email: alejandro.giraldo@titoma.com



	
		
