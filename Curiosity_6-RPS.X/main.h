#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <plib.h>


#define FW_VER "1.0.20191211"
#define AUTHOR "ALEJANDRO GM"
#define FALSE   0
#define TRUE    1
#define BAUD_RATE_1 115200

//#define SYS_FREQ 96000000
#define TOGGLES_PER_SEC     1000
#define PB_DIV  1
//Prescale 1, 8, 64, 256
#define PRESCALE    8
#define T1_TICK_RATE      ( SYS_FREQ / PB_DIV /PRESCALE / TOGGLES_PER_SEC ) 

#define CONFIG          ( CND_ON )
#define PINS            ( CND6_ENABLE )
#define PULLUPS         ( CND_PULLUP_DISABLE_ALL )
#define INTERRUPT       ( CHANGE_INT_ON | CHANGE_INT_PRI_1 )

//Time by default to turn on each led (0---65535)
#define TIME_ON_IN_MS     1000
////Time in millisecons to turn on the heartbeat led (0---65535)
#define TIME_TOGGLE_IN_MS 500

#define SEQUENCE_LED_RED    1
#define SEQUENCE_LED_GREEN    2
#define SEQUENCE_LED_YELLOW    3


//#define PARAM1  ADC_MODULE_ON | ADC_FORMAT_INTG | ADC_CLK_AUTO |  ADC_AUTO_SAMPLING_ON
//#define PARAM2  ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF | ADC_SAMPLES_PER_INT_2 | ADC_ALT_BUF_ON | ADC_ALT_INPUT_ON
//#define PARAM3  ADC_CONV_CLK_INTERNAL_RC | ADC_SAMPLE_TIME_12
//#define PARAM4 SKIP_SCAN_AN14
//#define PARAM5 ENABLE_AN14_ANA

// Shadow Register Set Priority Select (SRS Priority 7)
#pragma config FSRSSEL = PRIORITY_7     
// Peripheral Module Disable Configuration (Allow multiple reconfigurations)
#pragma config PMDL1WAY = OFF         
// Peripheral Pin Select Configuration (Allow multiple reconfigurations)
#pragma config IOL1WAY = OFF            
// USB USID Selection (Controlled by the USB Module)
#pragma config FUSBIDIO = ON           
// USB VBUS ON Selection (Controlled by USB Module)
#pragma config FVBUSONIO = ON        

/// DEVCFG2
// PLL Input Divider (5x Divider)
#pragma config FPLLIDIV = DIV_5 
// PLL Multiplier (24x Multiplier)
#pragma config FPLLMUL = MUL_24         
// USB PLL Input Divider (12x Divider)
#pragma config UPLLIDIV = DIV_12        
// USB PLL Enable (Disabled and Bypassed)
#pragma config UPLLEN = OFF           
// System PLL Output Clock Divider (PLL Divide by 1)
#pragma config FPLLODIV = DIV_1         

/// DEVCFG1
// Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FNOSC = PRIPLL  
// Secondary Oscillator Enable (Disabled)
#pragma config FSOSCEN = OFF         
// Internal/External Switch Over (Disabled)
#pragma config IESO = OFF               
// Primary Oscillator Configuration (HS osc mode)
#pragma config POSCMOD = HS             
// CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config OSCIOFNC = OFF           
// Peripheral Clock Divisor (Pb_Clk is Sys_Clk/1)
#pragma config FPBDIV = DIV_1         
// Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config FCKSM = CSDCMD       
// Watchdog Timer Postscaler (1:1048576)
#pragma config WDTPS = PS1048576       
// Watchdog Timer Window Enable (Watchdog Timer is in Non-Window Mode)
#pragma config WINDIS = OFF            
// Watchdog Timer Enable (WDT Disabled (SWDTEN Bit Controls))
#pragma config FWDTEN = OFF           
// Watchdog Timer Window Size (Window Size is 25%)
#pragma config FWDTWINSZ = WINSZ_25  

/// DEVCFG0
// Background Debugger Enable (Debugger is Disabled)
#pragma config DEBUG = OFF              
// JTAG Enable (JTAG Disabled)
#pragma config JTAGEN = OFF    
// ICE/ICD Comm Channel Select (Communicate on PGEC1/PGED1)
#pragma config ICESEL = ICS_PGx1       
// Program Flash Write Protect (Disable)
#pragma config PWP = OFF             
// Boot Flash Write Protect bit (Protection Disabled)
#pragma config BWP = OFF                
// Code Protect (Protection Disabled)
#pragma config CP = OFF                
/// #pragma config statements should precede project file includes.


/* *****************************************************************************
 End of File
 */
